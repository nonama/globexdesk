"""
WSGI config for globexdesk project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
import sys
import uwsgi
from django.utils import autoreload

sys.path.append('/home/env')
sys.path.append('/home/globexdesk')


root_path = os.path.abspath(os.path.split(__file__)[0])
sys.path.insert(0, os.path.join(root_path, 'globexdesk'))
sys.path.insert(0, root_path)

os.path.join(os.path.abspath(os.path.dirname(__file__)), '..')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "globexdesk.settings")

from django.core.wsgi import get_wsgi_application
import django.core.handlers.wsgi
application = get_wsgi_application()
application = django.core.handlers.wsgi.WSGIHandler()
