import django_filters
from models import Ticket

class ProductFilter(django_filters.FilterSet):
    class Meta:
        model = Ticket
        fields = {
            
            'price': ['lt', 'gt'],
            'release_date': ['exact', 'year__gt'],
        }